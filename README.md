# Building_manager

<div>
  
  <p>
    An android app to manage units of building.<br/>
    You can manage more than one building with this app . Also you can manage repairs , receipts and charge of each unit.<br/>
    <br/>
  </p>
  
  <big>Some feature of this app:</big><br/>
  <ul>
    <li>manage more than one building with one app</li>
    <li>handle charge , receipt and repairs of building</li>
    <li>send notification for people who lives in building</li>
    <li>show information of specific unit of each building</li>
  </ul>
  
  <img  src="images/1.png" width="200px" height="350px"/>&nbsp;
  <img  src="images/2.png" width="200px" height="350px"/>
  <br/>
  <img  src="images/3.png" width="200px" height="350px"/>&nbsp;
  <img  src="images/4.png" width="200px" height="350px"/>
  <br/>
  <img  src="images/5.png" width="200px" height="350px"/>&nbsp;
  <img  src="images/6.png" width="200px" height="350px"/>
  <br/>
  <img  src="images/7.png" width="200px" height="350px"/>
  
</div>
